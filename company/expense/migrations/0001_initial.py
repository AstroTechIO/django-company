# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('business_trip', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Accommodation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=255, db_index=True)),
                ('cost', models.DecimalField(decimal_places=2, max_digits=6)),
                ('date_start', models.DateTimeField(db_index=True)),
                ('date_end', models.DateTimeField(db_index=True)),
                ('with_breakfast', models.BooleanField(default=False)),
                ('with_lunch', models.BooleanField(default=False)),
                ('with_dinner', models.BooleanField(default=False)),
                ('attachment', models.FileField(upload_to='')),
                ('business_trip', models.ForeignKey(to='business_trip.BusinessTrip')),
            ],
            options={
                'verbose_name': 'Accommodation',
                'verbose_name_plural': 'Accommodations',
                'ordering': ['-date_start'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('date', models.DateTimeField(db_index=True, auto_now_add=True)),
                ('cost', models.DecimalField(decimal_places=2, max_digits=6)),
                ('tax_country_code', models.CharField(max_length=2, db_index=True, choices=[('PL', 'Poland')])),
                ('tax_id', models.CharField(max_length=10, db_index=True)),
                ('description', models.TextField()),
                ('attachment', models.FileField(upload_to='')),
            ],
            options={
                'verbose_name': 'Expense',
                'verbose_name_plural': 'Expenses',
                'ordering': ['-date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Travel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('method', models.CharField(max_length=10, choices=[('Plane', 'Plane'), ('Train', 'Train'), ('Car', 'Car'), ('Bus', 'Bus')])),
                ('start_place', models.CharField(max_length=50, db_index=True)),
                ('start_date', models.DateTimeField(db_index=True)),
                ('end_place', models.CharField(max_length=50, db_index=True)),
                ('end_date', models.DateTimeField(db_index=True)),
                ('cost', models.DecimalField(decimal_places=2, max_digits=6)),
                ('attachment', models.FileField(upload_to='')),
                ('business_trip', models.ForeignKey(to='business_trip.BusinessTrip')),
            ],
            options={
                'verbose_name': 'Travel',
                'verbose_name_plural': 'Travels',
                'ordering': ['-start_date'],
            },
            bases=(models.Model,),
        ),
    ]
