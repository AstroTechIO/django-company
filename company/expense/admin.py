from django.contrib import admin
from company.expense.models import Expense
from company.expense.models import Accommodation
from company.expense.models import Travel


@admin.register(Expense)
class ExpenseAdmin(admin.ModelAdmin):
    list_display = ['date', 'cost', 'tax_country_code', 'tax_id', 'description']
    list_display_links = ['description']
    list_filter = ['tax_country_code']
    search_fields = ['description', 'tax_id']


@admin.register(Travel)
class TravelAdmin(admin.ModelAdmin):
    list_display = ['method', 'business_trip', 'start_date', 'start_place', 'end_date', 'end_place']
    list_display_links = ['business_trip']
    search_fields = ['^start_place', '^end_place']
    list_filter = ['method', 'start_place', 'end_place']


@admin.register(Accommodation)
class AccomodationAdmin(admin.ModelAdmin):
    list_display = ['business_trip', 'name', 'cost', 'date_start', 'date_end', 'with_breakfast', 'with_lunch', 'with_dinner']
    list_display_links = ['name']
    search_fields = ['name']
    list_filter = ['with_breakfast', 'with_lunch', 'with_dinner']