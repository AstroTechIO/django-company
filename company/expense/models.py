from django.db import models
from django.utils.translation import ugettext_lazy as _


class Expense(models.Model):
    COUNTRIES = [
        ('PL', _('Poland'))]

    date = models.DateTimeField(auto_now_add=True, db_index=True)
    cost = models.DecimalField(decimal_places=2, max_digits=6)
    tax_country_code = models.CharField(choices=COUNTRIES, db_index=True, max_length=2)
    tax_id = models.CharField(max_length=10, db_index=True)
    description = models.TextField()
    attachment = models.FileField()

    def __str__(self):
        return '{date} {description}'.format(date=self.date, description=self.description)

    class Meta:
        ordering = ['-date']
        verbose_name = _('Expense')
        verbose_name_plural = _('Expenses')


class Accommodation(models.Model):
    business_trip = models.ForeignKey(to='business_trip.BusinessTrip', db_index=True)
    name = models.CharField(max_length=255, db_index=True)
    cost = models.DecimalField(max_digits=6, decimal_places=2)
    date_start = models.DateTimeField(db_index=True)
    date_end = models.DateTimeField(db_index=True)
    with_breakfast = models.BooleanField(default=False)
    with_lunch = models.BooleanField(default=False)
    with_dinner = models.BooleanField(default=False)
    attachment = models.FileField()

    def get_duration_in_hours(self):
        timedelta = self.date_end - self.date_start
        hours = timedelta.seconds / 60 / 60
        return round(hours, 2)

    def __str__(self):
        return '[{start_date} {end_date}] {name}'.format(
            start_date=self.date_start,
            end_date=self.date_end,
            name=self.name)

    class Meta:
        ordering = ['-date_start']
        verbose_name = _('Accommodation')
        verbose_name_plural = _('Accommodations')


class Travel(models.Model):
    METHODS = [
        ('Plane', _('Plane')),
        ('Train', _('Train')),
        ('Car', _('Car')),
        ('Bus', _('Bus'))]

    business_trip = models.ForeignKey(to='business_trip.BusinessTrip', db_index=True)
    method = models.CharField(max_length=10, choices=METHODS)
    start_place = models.CharField(max_length=50, db_index=True)
    start_date = models.DateTimeField(db_index=True)
    end_place = models.CharField(max_length=50, db_index=True)
    end_date = models.DateTimeField(db_index=True)
    cost = models.DecimalField(decimal_places=2, max_digits=6)
    attachment = models.FileField()

    def get_duration_in_hours(self):
        timedelta = self.end_date - self.start_date
        hours = timedelta.seconds / 60 / 60
        return round(hours, 2)

    def __str__(self):
        return '[{start_date} {end_date}] {start_place} - {end_place} '.format(
            start_date=self.start_date,
            start_place=self.start_place,
            end_date=self.end_date,
            end_place=self.end_place)

    class Meta:
        ordering = ['-start_date']
        verbose_name = _('Travel')
        verbose_name_plural = _('Travels')