# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BusinessTrip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('destination', models.CharField(max_length=255, db_index=True)),
                ('description', models.TextField()),
                ('date_start', models.DateTimeField(db_index=True)),
                ('date_end', models.DateTimeField(db_index=True)),
                ('total_cost', models.DecimalField(decimal_places=2, db_index=True, max_digits=6)),
                ('employee', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Business Trip',
                'verbose_name_plural': 'Business Trips',
                'ordering': ['-date_start'],
            },
            bases=(models.Model,),
        ),
    ]
