from django.db import models
from django.utils.translation import ugettext_lazy as _


class BusinessTrip(models.Model):
    destination = models.CharField(max_length=255, db_index=True)
    description = models.TextField()
    employee = models.ForeignKey(to='auth.User', db_index=True)
    date_start = models.DateTimeField(db_index=True)
    date_end = models.DateTimeField(db_index=True)
    total_cost = models.DecimalField(decimal_places=2, db_index=True, max_digits=6)

    def __str__(self):
        return '{start} - {end}'.format(
            start=self.date_start,
            end=self.date_end)

    class Meta:
        ordering = ['-date_start']
        verbose_name = _('Business Trip')
        verbose_name_plural = _('Business Trips')
