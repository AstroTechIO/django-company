from django.contrib import admin
from company.business_trip.models import BusinessTrip
from company.expense.models import Accommodation
from company.expense.models import Travel


class AccommodationInline(admin.TabularInline):
    model = Accommodation
    extra = 1
    inline_classes = ['open']


class TravelInline(admin.TabularInline):
    model = Travel
    extra = 2
    classes = ['collapse']


@admin.register(BusinessTrip)
class BusinessTripAdmin(admin.ModelAdmin):
    list_display = ['employee', 'date_start', 'date_end', 'total_cost', 'destination', 'description']
    list_display_links = ['employee']
    search_fields = ['^employee', '^destination', 'description']
    list_filter = ['employee', 'destination']
    raw_id_fields = ['employee']
    autocomplete_lookup_fields = {'fk': ['employee']}
    inlines = [AccommodationInline, TravelInline]
