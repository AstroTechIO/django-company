import sys
from setuptools import find_packages
from setuptools import setup


assert sys.version_info >= (3, 4), "Python 3.4+ required."


with open("requirements.txt") as file:
    install_requires = file.read().splitlines()


with open("README.rst") as file:
    long_description = file.read()


setup(
    name="django-spa-crm",
    description="Easy to use CRM for SPA - Sanitas Per Aquam and Swimming Pools.",
    long_description=long_description,
    license="Apache License 2.0",
    version="1.0.0",

    author="Matt Harasymczuk",
    author_email="code@mattagile.com",
    url="http://mattagile.com/",
    download_url="https://github.com/MattAgile/django-spa-crm",

    packages=find_packages(),
    package_data={"": ["LICENSE", "README.rst"], "crm": ["*.py"]},
    package_dir={"crm": "crm"},
    include_package_data=True,
    zip_safe=False,
    istall_requires=install_requires,

#    install_requires=[
#        'Django==1.6.6',
#        'django-grappelli==2.5.3',
#        'psycopg2==2.5.3'],

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Framework :: Django',
        'Intended Audience :: Healthcare Industry',
        'Intended Audience :: Other Audience',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Natural Language :: Polish',
        'Operating System :: OS Independent',
        'Programming Language :: JavaScript',
        'Programming Language :: Python :: 3.4',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application']
)



