==============
Django Company
==============


Project is Python / Django based.

.. image:: https://pypip.in/d/django-.../badge.png

Installation
============

There are two ways of install

- Vagrant install (recommended)
- System wide install


Vagrant install
===============

- Download and install Virtualbox_ == 4.3
- Download and install Vagrant_ == 1.6

.. code-block:: bash

    vagrant up

- Open your browser at http://localhost:8000/


System wide install
===================

- Download and install Python_ == 3.4
- Adjust ``company/settings.py`` and ``company/devsettings.py``

.. code-block:: bash

    pip install -r requirements.txt
    python manage.py migrate
    python manage.py collectstatic
    python manage.py test
    python manage.py runserver

- Open your browser at http://localhost:8000/


Build and Upload to PyPI
========================

.. code-block:: bash

    python setup.py register -r pypi
    python setup.py sdist upload -r pypi


Links
=====

.. _Python: https://www.python.org/downloads/
.. _Virtualbox: https://www.virtualbox.org/wiki/Downloads
.. _Vagrant: https://www.vagrantup.com/downloads.html

