FROM python:3.4
ENV PYTHONUNBUFFERED 1

RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y vim
RUN apt-get install -y wget
RUN apt-get install -y curl
RUN apt-get install -y nmap
RUN apt-get install -y htop
RUN apt-get install -y links
RUN apt-get install -y postgresql-client

RUN mkdir -p /var/www
WORKDIR /var/www
ADD . /var/www/
RUN pip install -r /var/www/requirements.txt

